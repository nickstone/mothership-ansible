# mothership-ansible

An additional role for splunk-ansible that includes some basic tasks to automatically stand up a Mothership Search Head with some environments and basic example search templates

splunk-ansible (https://github.com/splunk/splunk-ansible) is a pre-requisite for this to work. Event then, I probably modified something and have forgotten to add it here, so feel free to leave an issue or PR.